package lab5;

import java.awt.*;

/**
 * Class demonstrates methods from {@link aContainer}
 */
public class Main {

    public static void main(String[] args) {
        aContainer container1 = new aContainer();
        String string1 = new String();
        //Container's methods
        container1.add("asd");
        container1.add("fds");
        container1.add("asdf");
        container1.add("*nhds*");             //adding data to container (like array)
        container1.add("language");

        System.out.println("\nmethod size()");
        System.out.println("Size: " + container1.size());   //printing  data size 

        System.out.println("\nmethod toString()");
        System.out.println("toString: " + container1.toString());  //turning array into sti=ring 

        String[] s1 = container1.toArray();
        System.out.println("\nmethod toArray()");    //back to array 
        for (String s : s1) {
            System.out.println(s);
        }

        System.out.println("\nmethod contains()");
        System.out.println(container1.contains("Big"));

        System.out.println("\nmethod containsAll()");  //crating another container
        aContainer container2 = new aContainer();
        container2.add("asd");
        container2.add("fds");   //adding data to new container
        container2.add("asdf");
        container2.add("*nhds*");
        container2.add("language");
        System.out.println("Should return true");
        System.out.println("Result: " + container1.containsAll(container2));

        System.out.println("\nmethod remove()");  //removing data from container
        container1.remove("is");
        for (String s : container1) {
            System.out.println(s);
        }

        System.out.println("\nmethod clear()"); // clear
        container1.clear();
        for (String s : container1) {
            System.out.println(s);
        }

        // Using iterator's methods
        System.out.println("\nIterator's methods\n");
        aContainer.aIterator iterator = container2.iterator(); //iteration method choice
        for (String s : container2) {
            System.out.print(s + ' ');
        }
        System.out.println();
        if (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        iterator.remove();
        for (String s : container2) {
            System.out.print(s + ' ');
        }
        System.out.println();
        if (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        iterator.remove();
        for (String s : container2) {
            System.out.print(s + ' ');
        }
    }
}

