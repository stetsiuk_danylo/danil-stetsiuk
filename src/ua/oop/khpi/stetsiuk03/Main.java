
import java.util.Scanner; // импорт сканнера

public class Main   {
    public static void main(String args[]) {
        Scanner wk_string = new Scanner(System.in); //creating scanner 
        System.out.println("\nEnter string:"); 
        String string = wk_string.nextLine(); //creating string  and scanning input 
        char[] wk_char = string.toCharArray();
        int change=0 ; // change to num  and k to counter ex_char 
        int k=0 ; 
        int c_length = wk_char.length; // getting char length 
        int[] ex_char = new int[c_length];
        for(int i = 0 ; i<c_length;i++) {
            if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';') {  // checking for not alphabet symblos 
                continue; 
            }
            change=wk_char[i];
            ex_char[k]=change;
            k++;
            //System.out.println(change);   // debug 
        }
        for(int i=0;i<c_length;i++) {
            if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';'){ 
                continue ; 
            }
            System.out.print(wk_char[i]);      // printing start data with table sim
            System.out.print("  ");
        }
        System.out.println("");
        for(int i=0;i<c_length;i++){
                    if(ex_char[i]<91&&ex_char[i]>0) {
                        System.out.print(ex_char[i]);    // printing results using space  low index symbol
                        System.out.print("  ");
                    }
                    else if (ex_char[i]<=0) {break;}   //printing rusults using space High index symbol   
                    else {
                        System.out.print(ex_char[i]);
                        System.out.print("  ");
                    }
                    
        }
    }
}
     