package ua.oop.khpi.stetsiuk04;
import java.util.Scanner; // импорт сканнера

public class Main   {



         public static void debug(int change,char[] wk_char,int i) {
             System.out.println("Current char symbol:" +wk_char[i]);
             System.out.println("Current int symbol:" +change);
         }

         private static  void help() {
             System.out.println("@author : Danyl Stetsiuk CIT-120B\nversion : 1.0.3\nPurpose:Enter text. In the text, replace each letter with its number in the alphabet. Print the result as follows:") ;
             System.out.println("in one line print the text with two spaces between the letters, in the next line under each letter print its number.");
             System.out.println("");
         System.out.println("Case 1) Allows you to enter your data ;\nCase 2)Allows you to check data that saves in program;\nCase 3)Doing all operations over your data and saves result;\nCase 4)Showing results of mathematic operations;");
     }
    public static void main(String[] args) {
         String arg = args[0];
         if (arg.equals("-h") || arg.equals("-help")) {
             help();
         }
        System.out.println("Please choose task :");
        System.out.println("\n1)Enter data \n2)Check data\n3)Math operation\n4)Show result");
        Scanner tasks = new Scanner(System.in);
        int task = tasks.nextInt();
        System.out.println(task);
        Scanner wk_string = new Scanner(System.in); //creating scanner
        System.out.println("\nEnter string:");
        String string = wk_string.nextLine(); //creating string  and scanning input
        char[] wk_char = string.toCharArray();
        int change=0 ; // change to num  and k to counter ex_char
        int k=0 ;
        int c_length = wk_char.length; // getting char length
        int[] ex_char = new int[c_length];
        for(int i = 0 ; i<c_length;i++) {
            if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';') {
                continue;
            }
            change=wk_char[i];
            ex_char[k]=change;
            if (arg.equals("-d") || arg.equals("-debug")){
                debug(change,wk_char,i);
            }
            k++;
            //System.out.println(change);
        }
        for(int i=0;i<c_length;i++) {
            if(wk_char[i]==' ' || wk_char[i]==',' || wk_char[i]=='.' || wk_char[i]==';'){
                continue ;
            }
            System.out.print(wk_char[i]);
            System.out.print("  ");
        }
        while (task!=0) {
            System.out.println("Please choose task :");
            System.out.println("\n1)Enter data \n2)Check data\n3)Math operation\n4)Show result");
            task = tasks.nextInt();
            System.out.println(task);
            switch(task) {
                case 2:
                    System.out.println("\nYour string : " +string);//showing input result
                    break;
                case 3:
                    for(int i=0;i<c_length;i++){
                        if(ex_char[i]<91&&ex_char[i]>0) {
                            ex_char[i]=ex_char[i]-64;

                        }
                        else if (ex_char[i]<=0) {break;}
                        else {
                            ex_char[i]=ex_char[i]-96;
                        }
                    }
                    break;
                case 4:
                    for(int i=0;i<c_length;i++){
                        if(ex_char[i]<91&&ex_char[i]>0) {
                            System.out.print(ex_char[i]);
                            System.out.print("  ");
                        }
                        else if (ex_char[i]<=0) {break;}
                        else {
                            System.out.print(ex_char[i]);
                            System.out.print("  ");
                        }
                    }
                    break;
            }
        }
    }
}
